import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AdminComponent } from '@layout/admin/admin.component'
import { SiteComponent } from '@layout/site/site.component'
import { CleanComponent } from '@layout/clean/clean.component'

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  {
    path: 'waifu',
    component: AdminComponent,
    loadChildren: () => import('@pages/waifu/waifu.module').then(m => m.WaifuModule)
  },
  {
    path: 'article',
    component: SiteComponent,
    loadChildren: () => import('@pages/article/article.module').then(m => m.ArticleModule)
  },
  {
    path: 'login',
    component: CleanComponent,
    loadChildren: () => import('@pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'register',
    component: CleanComponent,
    loadChildren: () => import('@pages/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: '**',
    component: CleanComponent,
    loadChildren: () => import('@pages/error/error.module').then(m => m.ErrorModule)
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
