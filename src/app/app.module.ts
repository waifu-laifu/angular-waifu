import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { registerLocaleData } from '@angular/common'
import en from '@angular/common/locales/en'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { AdminComponent } from './layout/admin/admin.component'
import { SiteComponent } from './layout/site/site.component'
import { CleanComponent } from './layout/clean/clean.component'

import { IconsProviderModule } from './icons-provider.module'
import {
  NZ_I18N,
  en_US,
  NzLayoutModule,
  NzIconModule,
  NzMenuModule,
  NzBreadCrumbModule,
  NzTypographyModule,
  NzToolTipModule
} from 'ng-zorro-antd'

registerLocaleData(en)

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    SiteComponent,
    CleanComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IconsProviderModule,
    HttpClientModule,
    NzLayoutModule,
    NzIconModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzTypographyModule,
    NzToolTipModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
