class ThreeSize {
  bust: number | null
  waist: number | null
  hips: number | null
}

export class Waifu {
  name: string
  birth: Date | null
  age: number | null
  bwh: ThreeSize
  image: string
  series: string
  seiyuu: string | null
}


export const waifuTemplate: Waifu =
{
  name: '',
  birth: null,
  age: null,
  bwh: {
    bust: null,
    waist: null,
    hips: null
  },
  image: '',
  series: '',
  seiyuu: null
}
