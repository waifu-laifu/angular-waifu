import { Component, OnInit } from '@angular/core'
import { AdminMenu } from './admin-menu'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {

  isCollapsed = false
  menu: AdminMenu[] = [
    { text: 'Waifu', path: '/waifu' }
  ]
  currentYear: number = (new Date).getFullYear()

  constructor() { }

  ngOnInit() {
  }

}
