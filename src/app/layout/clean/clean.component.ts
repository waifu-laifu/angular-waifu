import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-clean',
  templateUrl: './clean.component.html',
  styleUrls: ['./clean.component.less']
})
export class CleanComponent implements OnInit {

  constructor() { }

  currentYear: number = (new Date).getFullYear()

  ngOnInit() {
  }

}
