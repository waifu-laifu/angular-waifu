import { Waifu } from '@class/waifu'

const WAIFUS: Waifu[] = [
  {
    name: 'Akame',
    birth: null,
    age: null,
    bwh: {
      bust: 81,
      waist: 56,
      hips: 83
    },
    image: 'https://cdn.myanimelist.net/images/characters/2/323665.jpg',
    series: 'Akame ga Kill!',
    seiyuu: 'Sora Amamiya'
  },
  {
    name: 'Rem',
    birth: new Date('1995-02-02'),
    age: 24,
    bwh: {
      bust: null,
      waist: null,
      hips: null
    },
    image: 'https://cdn.myanimelist.net/images/characters/10/311627.jpg',
    series: 'Re:Zero Kara Hajimeru Isekai Seikatsu',
    seiyuu: 'Inori Minase'
  },
  {
    name: 'Aoba Suzukaze',
    birth: new Date('1995-02-02'),
    age: 24,
    bwh: {
      bust: null,
      waist: null,
      hips: null
    },
    image: 'https://cdn.myanimelist.net/images/characters/5/313540.jpg',
    series: 'New Game!!',
    seiyuu: 'Yuuki Takada'
  },
  {
    name: 'Vignette Tsukinose April',
    birth: new Date('1997-10-10'),
    age: 22,
    bwh: {
      bust: 80,
      waist: 57,
      hips: 82
    },
    image: 'https://cdn.myanimelist.net/images/characters/7/310288.jpg',
    series: 'Gabriel DropOut',
    seiyuu: 'Saori Oonishi'
  },
  {
    name: 'Rio Futaba',
    birth: new Date('1997-10-23'),
    age: 22,
    bwh: {
      bust: null,
      waist: null,
      hips: null
    },
    image: 'https://cdn.myanimelist.net/images/characters/11/361763.jpg',
    series: 'Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai',
    seiyuu: 'Atsumi Tanezaki'
  },
  {
    name: 'Yuuko Yoshida',
    birth: new Date('1999-09-28'),
    age: 24,
    bwh: {
      bust: null,
      waist: null,
      hips: null
    },
    image: 'https://cdn.myanimelist.net/images/characters/8/388684.jpg',
    series: 'Machikado Mazoku',
    seiyuu: 'Konomi Kohara'
  },
]

export { WAIFUS }
