import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ArticleRoutingModule } from './article-routing.module'

import { ArticleComponent } from './article.component'


@NgModule({
  imports: [ArticleRoutingModule, CommonModule],
  declarations: [ArticleComponent],
  exports: [ArticleComponent]
})
export class ArticleModule { }
