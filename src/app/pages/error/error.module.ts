import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ErrorRoutingModule } from './error-routing.module'

import { ErrorComponent } from './error.component'

import {
  NzTypographyModule,
  NzGridModule
} from 'ng-zorro-antd'

@NgModule({
  imports: [
    ErrorRoutingModule,
    CommonModule,
    NzTypographyModule,
    NzGridModule
  ],
  declarations: [ErrorComponent],
  exports: [ErrorComponent]
})
export class ErrorModule { }
