import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { LoginRoutingModule } from './login-routing.module'

import { LoginComponent } from './login.component'

import {
  NzCardModule,
  NzFormModule,
  NzCheckboxModule,
  NzInputModule,
  NzButtonModule
} from 'ng-zorro-antd'

@NgModule({
  imports: [
    LoginRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzCardModule,
    NzFormModule,
    NzCheckboxModule,
    NzInputModule,
    NzButtonModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})
export class LoginModule { }
