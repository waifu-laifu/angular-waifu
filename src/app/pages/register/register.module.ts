import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { RegisterRoutingModule } from './register-routing.module'

import { RegisterComponent } from './register.component'

import {
  NzCardModule,
  NzFormModule,
  NzCheckboxModule,
  NzInputModule,
  NzButtonModule
} from 'ng-zorro-antd'



@NgModule({
  imports: [
    RegisterRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzCardModule,
    NzFormModule,
    NzCheckboxModule,
    NzInputModule,
    NzButtonModule
  ],
  declarations: [RegisterComponent],
  exports: [RegisterComponent]
})
export class RegisterModule { }
