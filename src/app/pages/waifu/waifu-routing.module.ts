import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { WaifuComponent } from './waifu.component'

const routes: Routes = [
  { path: '', component: WaifuComponent },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WaifuRoutingModule { }
