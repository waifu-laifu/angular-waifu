import { Component, OnInit } from '@angular/core'

import { Waifu, waifuTemplate } from '@class/waifu'
import { WaifuService } from './waifu.service'

import { NzModalService } from 'ng-zorro-antd'

@Component({
  selector: 'app-waifu',
  templateUrl: './waifu.component.html',
  styleUrls: ['./waifu.component.less']
})
export class WaifuComponent implements OnInit {

  waifus: Waifu[]
  waifu: Waifu | null
  previewVisible: boolean = false
  previewUrl: string | null
  previewWaifuName: string | null
  editedIndex: number = -1
  deletedIndex: number = -1
  actionVisible: boolean = false
  detailVisible: boolean = false

  addCondition(): boolean {
    return this.editedIndex < 0 && this.deletedIndex < 0
  }
  editCondition(): boolean {
    return this.editedIndex >= 0 && this.deletedIndex < 0
  }
  deleteCondition(): boolean {
    return this.editedIndex < 0 && this.deletedIndex >= 0
  }
  modalTitle(): string {
    if (this.addCondition()) {
      return 'Add Waifu'
    } else if (this.editCondition()) {
      return 'Edit Waifu'
    } else if (this.deleteCondition()) {
      return 'Delete Waifu'
    }
  }
  getWaifus(): void {
    this.waifuService.getWaifus()
      .subscribe(waifus => this.waifus = waifus)
  }
  detailWaifu(id: number): void {
    this.waifuService.detailWaifu(id)
      .subscribe(waifu => this.waifu = waifu)
  }
  addWaifu(waifu: Waifu): void {
    this.waifuService.addWaifu(waifu)
    this.getWaifus()
  }
  editWaifu(waifu: Waifu, id: number): void {
    this.waifuService.editWaifu(waifu, id)
    this.getWaifus()
  }
  deleteWaifu(id: number): void {
    this.waifuService.deleteWaifu(id)
    this.getWaifus()
  }
  previewWaifu(id: number) {
    this.previewUrl = this.waifus[id].image
    this.previewWaifuName = this.waifus[id].name.split(' ')[0]
    this.previewVisible = true
  }
  detailModal(id: number): void {
    this.detailWaifu(id)
    this.detailVisible = true
  }
  addModal(): void {
    this.waifu = {...waifuTemplate}
    this.actionVisible = true
  }
  editModal(id: number): void {
    this.detailWaifu(id)
    this.editedIndex = id
    this.actionVisible = true
  }
  deleteModal(id: number, name: string): void {
    this.deletedIndex = id
    this.modalService.confirm({
      nzTitle: this.modalTitle(),
      nzContent: `Do You want to delete ${name} ?`,
      nzOnCancel: () => this.closeModal(),
      nzOnOk: () => this.deleteWaifu(this.deletedIndex)
    })
  }
  closeModal(): void {
    this.previewVisible = false
    this.actionVisible = false
    this.detailVisible = false
    setTimeout(() => {
      this.previewUrl = null
      this.waifu = waifuTemplate
      this.editedIndex = -1
      this.deletedIndex = -1
    }, 500)
  }
  saveWaifu(waifu: Waifu): void {
    if (this.addCondition()) {
      this.addWaifu(waifu)
    } else if (this.editCondition()) {
      this.editWaifu(waifu, this.editedIndex)
    } else if (this.deleteCondition()) {
      this.deleteWaifu(this.deletedIndex)
    }
    this.closeModal()
  }

  constructor(private waifuService: WaifuService, private modalService: NzModalService) { }

  ngOnInit() {
    this.getWaifus()
  }

}
