import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { WaifuRoutingModule } from './waifu-routing.module'

import { WaifuComponent } from './waifu.component'

import {
  NzGridModule,
  NzCardModule,
  NzIconModule,
  NzButtonModule,
  NzModalModule,
  NzPageHeaderModule,
  NzInputModule,
  NzDatePickerModule,
  NzTypographyModule
} from 'ng-zorro-antd'


@NgModule({
  imports: [
    WaifuRoutingModule,
    CommonModule,
    FormsModule,
    NzGridModule,
    NzCardModule,
    NzIconModule,
    NzButtonModule,
    NzModalModule,
    NzPageHeaderModule,
    NzInputModule,
    NzDatePickerModule,
    NzTypographyModule
  ],
  declarations: [WaifuComponent],
  exports: [WaifuComponent]
})
export class WaifuModule { }
