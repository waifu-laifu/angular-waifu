import { TestBed } from '@angular/core/testing';

import { WaifuService } from './waifu.service';

describe('WaifuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WaifuService = TestBed.get(WaifuService);
    expect(service).toBeTruthy();
  });
});
