import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Waifu } from '@class/waifu'

import { WAIFUS } from '@mock-data'

@Injectable({
  providedIn: 'root'
})
export class WaifuService {

  getWaifus(): Observable<Waifu[]> {
    return of(WAIFUS)
  }

  detailWaifu(id: number): Observable<Waifu> {
    return of(WAIFUS[id])
  }

  addWaifu(waifu: Waifu): void {
    WAIFUS.push(waifu)
  }

  editWaifu(waifu: Waifu, id: number): void {
    WAIFUS.splice(id, 1, waifu)
  }

  deleteWaifu(id: number): void {
    WAIFUS.splice(id, 1)
  }

  constructor() { }
}
